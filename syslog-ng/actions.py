#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 2.
# See the file http://www.gnu.org/copyleft/gpl.txt.

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
from pisi.actionsapi import get


def setup():
    autotools.configure("--prefix=/usr \
                         --sysconfdir=/etc/syslog-ng \
                         --libexecdir=/usr/lib \
                         --sbindir=/usr/bin \
                         --localstatedir=/var/lib/syslog-ng \
                         --datadir=/usr/share \
                         --with-pidfile-dir=/var/run \
                         --enable-ipv6 \
                         --with-systemdsystemunitdir=no \
                         --enable-manpages \
                         --enable-all-modules \
                         --enable-spoof-source \
                         --disable-java \
                         --disable-java-modules  \
                         --disable-riemann \
                         --disable-kafka \
                         --with-python=3 \
                         --disable-mongodb \
                         --enable-sql \
                         --disable-smtp \
                         --disable-redis \
                         --disable-amqp \
                         --disable-geoip2 \
                         --with-jsonc=system")

def build():
    autotools.make()

def install():
    autotools.rawInstall("DESTDIR=%s" % get.installDIR())

    pisitools.dodoc("AUTHORS", "COPYING", "README*", "NEWS*")
