#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import shelltools
from pisi.actionsapi import get
from pisi.actionsapi import pisitools

def setup():
    #shelltools.system("sed 's/os-release/lsb-release/' -i src/test/test-copy.c")
    shelltools.makedirs("build")
    shelltools.cd("build")
    shelltools.system("meson .. -Drootlibdir=/usr/lib \
                                -Dprefix=/usr \
                                -Drootlibexecdir=/usr/lib/elogind \
                                -Ddefault-hierarchy=unified \
                                -Ddbuspolicydir=/usr/share/dbus-1/system.d \
                                -Dpamlibdir=/lib/security \
                                -Ddocdir=/usr/share/doc/elogind \
                                -Dhalt-path=/sbin/halt \
                                -Dpoweroff-path=/sbin/poweroff \
                                -Dreboot-path=/sbin/reboot \
                                -Ddefault-kill-user-processes=false \
                                -Dcgroup-controller=elogind \
                                -Dsplit-bin=true \
                                -Dpolkit=true \
                                -Dsplit-usr=false")
def build():
    shelltools.cd("build")
    shelltools.system("ninja")

def install():
    shelltools.cd("build")
    shelltools.system("DESTDIR=%s ninja install" % get.installDIR())
    #pisitools.remove("/lib/udev/rules.d/70-power-switch.rules")

    shelltools.cd("..")
    pisitools.dodoc("LICENSE*", "README*")

