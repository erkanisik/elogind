# -*- coding: utf-8 -*-
from comar.service import *
import os

serviceType="local"
serviceDesc=_({"en": "An IPC message bus daemon",
               "tr": "An IPC message bus daemon"})
serviceDefault="on"
PIDFILE="/var/run/dbus.pid"
DAEMON="/usr/bin/dbus-daemon"

@synchronized
def start():
    startService(command=DAEMON,
                 pidfile=PIDFILE,
                 args="--system",
                 detach=True,
                 donotify=True)
    

@synchronized
def stop():
    stopService(pidfile=PIDFILE,
                donotify=True)

    try:
        os.unlink(PIDFILE)
    except OSError:
        pass

def status():
    return isServiceRunning(PIDFILE)
