#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright 2017 TUBITAK/UEKAE
# Licensed under the GNU General Public License, version 2.
# See the file http://www.gnu.org/copyleft/gpl.txt.

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import shelltools
from pisi.actionsapi import get


def setup():
    shelltools.makedirs("build")
    shelltools.cd("build")
    
    shelltools.system("meson .. --prefix=/usr \
                                --sysconfdir=/etc \
                                --localstatedir=/var \
                                -Dlibexecdir=/usr/lib/fwupd \
                                -Dsystemd=false \
                                -Dconsolekit=false \
                                -Delogind=true \
                                -Dplugin_dell=false \
                                -Dplugin_synaptics=false \
                                -Dplugin_uefi=false \
                                -Dplugin_tpm=false \
                                -Dplugin_nvme=false \
                                -Defi-includedir=/usr/include/efi \
                                -Dplugin_thunderbolt=false \
                                -Dgtkdoc=false \
                                -Dtests=true \
                                -Dplugin_flashrom=false \
                                -Dplugin_thunderbolt=true \
                                -Dplugin_modem_manager=true \
                                -Dman=false")
    

def build():
    #pisitools.dosed("libfwupdplugin/fu-device.c","@fw:","@firmware:")
    shelltools.cd("build")
    shelltools.system("ninja")
    

def install():
    shelltools.cd("build")
    
    shelltools.system("DESTDIR=%s ninja install" % get.installDIR())

    #pisitools.dodoc("AUTHORS", "ChangeLog", "README*", "NEWS")
